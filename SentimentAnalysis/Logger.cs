﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SentimentAnalysis
{
    public static class Logger
    {
        private static string logFilePath = "LogFile.txt";

        public static string LogFilePath
        {
            get { return logFilePath; }
            set { logFilePath = value; }
        }

        public static void Log(string message)
        {
            File.AppendAllText(logFilePath, Environment.NewLine + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":\t" + message + Environment.NewLine);
            Console.WriteLine(Environment.NewLine + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":\t" + message + Environment.NewLine);
        }

        public static void Log(Exception ex)
        {
            File.AppendAllText(logFilePath, Environment.NewLine + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":\t" + ex.Message + Environment.NewLine + "Inner Execption: " + ex.InnerException + Environment.NewLine + ex.ToString() + Environment.NewLine);
            Console.WriteLine(Environment.NewLine + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ":\t" + ex.Message + Environment.NewLine + "Inner Execption: " + ex.InnerException + Environment.NewLine + ex.ToString() + Environment.NewLine);

        }


    }
}

