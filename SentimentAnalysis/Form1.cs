﻿using CsvHelper;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace SentimentAnalysis
{
    public partial class Form1 : Form
    {
        public enum HTTP_TYPE { NONE, GET, POST, PUT };
        static string apiKey = "apikey=50c808d2-3293-4a02-adaa-c9f9da91a051";
        DataTable dt = new DataTable();
        static List<SentimentResult> listSR = new List<SentimentResult>();
        public static int count = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog1.ShowDialog();

            openFileDialog1.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                textBoxFileName.Text = file;
                try
                {
                    //string text = File.ReadAllText(file);
                    ReadCsv(file);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.ToString());
                    Logger.Log(ex);
                }
            }
        }


        public static ResponseData GetRequestIdol(string resourceName)
        {
            ResponseData responseData = new ResponseData();

            try
            {
                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.idolondemand.com/1/api/sync/");


                var request = new RestRequest();
                request.Resource = resourceName;

                var response = client.Execute(request) as RestResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseData.Code = response.StatusCode.ToString();
                    responseData.Content = response.Content;
                    responseData.IsSuccess = true;
                    responseData.RestResponse = response;
                    return responseData;
                }
                else
                {
                    //NOK
                    Logger.Log(response.Content);
                    responseData.RestResponse = response;
                    responseData.IsSuccess = false;
                    return responseData;
                }
            }
            catch (Exception ex)
            {
                responseData.IsSuccess = false;
                Logger.Log(ex);
                return responseData;
            }
        }


        public void ReadCsv(string filePath)
        {
            try
            {
                
                dt.Columns.Add("textColumn", typeof(String));

                TextReader textReader = File.OpenText(filePath);
                var csv = new CsvReader(textReader);

                while (csv.Read())
                {
                    var row = dt.NewRow();
                    foreach (DataColumn column in dt.Columns)
                    {
                        row[column.ColumnName] = csv.GetField(column.DataType, column.ColumnName);
                    }
                    dt.Rows.Add(row);
                }
                
                dataGridView1.DataSource = dt;
                count = dt.Rows.Count - 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error is " + ex.ToString());
                Logger.Log(ex);
            }
        }

        public static void GetRequestIdolAsync(string text)
        {
            ResponseData responseData = new ResponseData();

            try
            {
                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.idolondemand.com/1/api/sync/");

                //Get
                string escapedText = Uri.EscapeDataString(text);
                var request = new RestRequest("analyzesentiment/v1?text="+ escapedText + "&language=eng&apikey=50c808d2-3293-4a02-adaa-c9f9da91a051", Method.GET);
                //request.RequestFormat = DataFormat.Json;
                client.ExecuteAsync(request, (response) => {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine("{0}", response.Content);
                        RootObject result = JsonConvert.DeserializeObject<RootObject>(response.Content);
                        SentimentResult sr = new SentimentResult();
                        if (result.negative.Count > 0)
                        {
                            sr.original_text = result.negative[0].original_text;
                            sr.sentimentAggregate = result.aggregate.sentiment;
                            sr.scoreAggregate = result.aggregate.score;
                        }
                        if (result.positive.Count > 0)
                        {
                            sr.original_text = result.positive[0].original_text;
                            sr.sentimentAggregate = result.aggregate.sentiment;
                            sr.scoreAggregate = result.aggregate.score;
                        }
                        if (result.positive.Count == 0 && result.negative.Count == 0)
                        {
                            sr.original_text = text;
                            sr.sentimentAggregate = result.aggregate.sentiment;
                            sr.scoreAggregate = result.aggregate.score;
                        }
                        WriteResults(sr);
                        listSR.Add(sr);

                    }
                    else {
                        Console.WriteLine("{0} {1}", response.StatusCode, response.ErrorMessage);
                    }
                });
            }
            catch (Exception ex)
            {
                responseData.IsSuccess = false;
                Logger.Log(ex);
                MessageBox.Show(ex.ToString());

            }
        }

        public static void WriteResults(SentimentResult sr)
        {
            File.AppendAllText("Results.csv", Environment.NewLine + "\"" + sr.original_text +"\"" + "," + sr.sentimentAggregate + "," + sr.scoreAggregate );
        }

        public static string GetResultsFilePath()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", ""); ;
            string filePath = System.IO.Path.Combine(appPath, "Results.csv");

            if (!System.IO.File.Exists(filePath))
            {
                string message = "Missing Results file: " + filePath;
                MessageBox.Show(message);
            }
            return filePath;
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in dt.Rows)
            {
                string text = row["textColumn"].ToString(); ;
                    //row.Cells["textColumn"].Value.ToString();
                GetRequestIdolAsync(text);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string file = GetResultsFilePath();
                if (File.Exists(file))
                    System.Diagnostics.Process.Start(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Logger.Log(ex);
            }

        }
    }


    public class ResponseData
    {
        public string Code { get; set; }
        public string Content { get; set; }
        public string LwSsoCookie { get; set; }
        public IRestResponse RestResponse { get; set; }
        public Headers HeadersArray { get; set; }
        public bool IsSuccess { get; set; }

        public ResponseData()
        {
            HeadersArray = new Headers();
            IsSuccess = false;
        }

    }

    public class Header
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Headers
    {
        public List<Header> HeadersList { get; set; }

        public Headers()
        {
            HeadersList = new List<Header>();
        }

        public new string ToString()
        {
            string result = "";
            if (HeadersList == null || HeadersList.Count == 0)
                return "";
            foreach (var Header in HeadersList)
            {
                result += Header.Name + "=" + Header.Value + ";";
            }

            return result;
        }
    }

    public class Positive
    {
        public string sentiment { get; set; }
        public string topic { get; set; }
        public double score { get; set; }
        public string original_text { get; set; }
        public int original_length { get; set; }
        public string normalized_text { get; set; }
        public int normalized_length { get; set; }
    }

    public class Negative
    {
        public string sentiment { get; set; }
        public string topic { get; set; }
        public double score { get; set; }
        public string original_text { get; set; }
        public int original_length { get; set; }
        public string normalized_text { get; set; }
        public int normalized_length { get; set; }
    }

    public class Aggregate
    {
        public string sentiment { get; set; }
        public double score { get; set; }
    }

    public class RootObject
    {
        public List<Positive> positive { get; set; }
        public List<Negative> negative { get; set; }
        public Aggregate aggregate { get; set; }
    }

    public class SentimentResult
    {
        public string sentimentAggregate { get; set; }
        public double scoreAggregate { get; set; }
        public string sentiment { get; set; }
        public string topic { get; set; }
        public double score { get; set; }
        public string original_text { get; set; }
        public int original_length { get; set; }
        public string normalized_text { get; set; }
        public int normalized_length { get; set; }
    }
}
